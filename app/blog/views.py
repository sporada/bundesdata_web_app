from django.shortcuts import render


def home(request):
    """
    This view creates the homepage of the web app.
    """
    return render(request, "blog/home.html", {"title": "Homepage"})


def blog(request):
    """
    This view creates a blog page. Not used right now.
    """
    return render(request, "blog/blog.html")


def about(request):
    """
    This view creates the abot/info page of the web app.
    """
    return render(request, "blog/about.html", {"title": "About"})


def impressum(request):
    """
    This view creates the impressum page of the web app.
    """
    return render(request, "blog/impressum.html", {"title": "Impressum"})
