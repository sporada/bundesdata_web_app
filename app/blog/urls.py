from django.urls import path
from . import views

"""
Url paths for all blog views.
"""

urlpatterns = [
    path("blog/", views.blog, name="blog"),
    path("about/", views.about, name="about-page"),
    path("", views.home, name="home"),
    path("impressum/", views.impressum, name="impressum")
]
