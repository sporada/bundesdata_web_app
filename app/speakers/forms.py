from django import forms


class SearchForm(forms.Form):
    """
    Configures the html input form for the speaker search.
    """
    query = forms.CharField(label="Suche MdB", max_length="200")

    query.widget.attrs.update({"class": "autocomplete materialize-textarea",
                               "id": "icon_prefix2"})
