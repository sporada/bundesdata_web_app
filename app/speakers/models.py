from django.db import models


class Speaker(models.Model):
    """
    This models contains general data about one MdB. Data will be imported from
    the Stammdatenbank.xml via the custom django-admin command import_speakers.py.
    """
    id = models.IntegerField(verbose_name="MdB ID", primary_key=True)
    last_name = models.CharField(verbose_name="Nachname", max_length=50)
    first_name = models.CharField(verbose_name="Vorname", max_length=50)
    nobility = models.CharField(verbose_name="Adelstitel", max_length=50,
                                null=True)
    name_prefix = models.CharField(verbose_name="Namenspräfix", max_length=50,
                                   null=True)
    title = models.CharField(verbose_name="Akademischer Titel", null=True,
                             blank=True, max_length=50)
    birthday = models.IntegerField(verbose_name="Geburtstag", )
    birthplace = models.CharField(verbose_name="Geburtsort", null=True,
                                  blank=True, max_length=50)
    country_of_birth = models.CharField(verbose_name="Geburtsland", null=True,
                                        blank=True, max_length=50)
    day_of_death = models.IntegerField(verbose_name="Todesjahr", null=True,
                                       blank=True)

    occupation = models.TextField(verbose_name="Beruf")
    short_vita = models.TextField(verbose_name="Kurzbiographie", default=None,
                                  null=True, blank=True)
    party = models.CharField(verbose_name="Partei", null=True, blank=True,
                             max_length=50)

    def __str__(self):
        return str(self.id) + " " + self.first_name + " " + self.last_name


class LegislativeInfo(models.Model):
    """
    This model contains data about the periods an MdB was an active part of the
    Deutsche Bundestag. Needs a foreign key which is the coresponding Speaker
    entry.
    """
    foreign_speaker = models.ForeignKey("Speaker", on_delete=models.CASCADE)
    legislative_period = models.IntegerField(verbose_name="Wahlperiode",
                                             null=True)
    legislative_period_start_date = models.DateField(verbose_name="MdB von",
                                                     null=True)
    legislative_period_end_date = models.DateField(verbose_name="MdB bis",
                                                   null=True)
    mandate_type = models.CharField(verbose_name="Mandatsart", null=True,
                                    blank=True, max_length=50)

    def __str__(self):
        return str(self.foreign_speaker) + " " + str(self.legislative_period)


class LegislativeInstitution(models.Model):
    """
    This model contains data about the instituions an MdB was part of during a
    specific legislative period. Needs a foreign key which is the coresponding
    Speaker entry.
    """
    foreign_speaker = models.ForeignKey("Speaker",
                                        on_delete=models.CASCADE)
    current_period = models.IntegerField(verbose_name="Wahlperiode",
                                         null=True)
    institution = models.CharField(verbose_name="Institut", null=True,
                                   blank=True, max_length=255)
    institution_start_date = models.DateField(verbose_name="Mitglied von",
                                              null=True)
    institution_end_date = models.DateField(verbose_name="Mitglied bis",
                                            null=True)

    def __str__(self):
        return str(self.foreign_legislative_info) + " " + str(self.institution)
