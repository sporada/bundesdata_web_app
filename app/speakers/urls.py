from django.urls import path
from . import views

"""
Url paths for all speakers views.
"""

urlpatterns = [
    path("", views.speakers, name="MdBs"),
    path("mdb/<int:id>", views.speaker, name="MdB"),
]
