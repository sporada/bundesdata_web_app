from django.apps import AppConfig
from watson import search as watson


class SpeakersConfig(AppConfig):
    name = 'speakers'

    def ready(self):
        Speaker = self.get_model("Speaker")
        watson.register(Speaker, exclude=["short_vita"])
