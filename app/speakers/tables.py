import django_tables2 as tables
from .models import Speaker
from django_tables2.utils import A  # alias for Accessor


class SpeakerTable(tables.Table):
    """
    Configures the table showing all speakers. Inserts a column with links to
    the profile of one speaker. Also defines all shown columns. The template
    speakers/table.html is imported in line 19.
    """
    link = tables.LinkColumn("MdB", text="Profil", args=[A("id")],
                             orderable=False,
                             attrs={"a": {"class": "waves-effect waves-light btn light-green darken-3"}})  # Adds colum with Link to Profile

    class Meta:
        model = Speaker
        fields = ("last_name", "first_name", "party", "id")
        template_name = "speakers/table.html"
        empty_text = ("Für den eingegebenen Suchbegriff gibt es leider keine Ergebnisse.")
