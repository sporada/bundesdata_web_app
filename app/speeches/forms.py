from django import forms


class SearchForm(forms.Form):
    """
    Configures the html input form for the protocol search.
    """
    query = forms.CharField(label="Suche Protokoll", max_length="200")

    query.widget.attrs.update({"class": "autocomplete materialize-textarea",
                               "id": "icon_prefix2"})


class SearchFormSpeech(forms.Form):
    """
    Configures the html input form for the speech search.
    """
    query = forms.CharField(label="Suche Rede", max_length="200")

    query.widget.attrs.update({"class": "autocomplete materialize-textarea",
                               "id": "icon_prefix2"})
