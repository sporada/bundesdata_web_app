from django.urls import path
from . import views

"""
Url paths for all speeches views.
"""

urlpatterns = [
    path("reden/", views.speeches, name="Reden"),
    path("liste-protokolle/", views.protocols, name="Protokoll-list"),
    path("protokoll/<int:protocol_id>", views.protocol, name="Protokoll"),
    path("rede/<str:speech_id>", views.speech, name="Rede")
]
