import django_tables2 as tables
from .models import Speech, Protocol
from django_tables2.utils import A  # alias for Accessor


class SpeechTable(tables.Table):
    """
    Configures the table showing all speeches. Inserts a column with links to
    the speeches. Also defines all shown columns. The template
    speeches/table.html is imported in line 21.
    """
    link = tables.LinkColumn("Rede", text="Rede", args=[A("speech_id")],
                             orderable=False,
                             attrs={"a": {"class": "waves-effect waves-light btn light-green darken-3"}})  # Adds colum with Link to Rede

    class Meta:
        model = Speech
        fields = ("speech_id", "foreign_protocol.protocol_id",
                  "foreign_protocol.session_date", "foreign_speaker.id",
                  "foreign_speaker.first_name", "foreign_speaker.last_name")
        template_name = "speeches/table.html"
        empty_text = ("Für den eingegebenen Suchbegriff gibt es leider keine Ergebnisse.")


class SpeakerSpeechTable(tables.Table):
    """
    Configures the table showing all speeches of one speaker in his profile.
    Inserts a column with links to the speeches. Also defines all shown columns.
    The template
    speeches/table.html is imported in line 39.
    """
    link = tables.LinkColumn("Rede", text="Rede", args=[A("speech_id")],
                             orderable=False,
                             attrs={"a": {"class": "waves-effect waves-light btn light-green darken-3"}})  # Adds colum with Link to Speaker

    class Meta:
        model = Speech
        fields = ("speech_id", "foreign_protocol.protocol_id", "foreign_protocol.session_date")
        template_name = "speeches/table.html"
        empty_text = ("Für den eingegebenen Suchbegriff gibt es leider keine Ergebnisse.")


class ProtocolTable(tables.Table):
    """
    Configures the table showing all protocols.
    Inserts a column with links to the protocols. Also defines all shown columns.
    The template
    speeches/table.html is imported in line 57.
    """
    link = tables.LinkColumn("Protokoll", text="Protokoll", args=[A("protocol_id")],
                             orderable=False,
                             attrs={"a": {"class": "waves-effect waves-light btn light-green darken-3"}})  # Adds colum with Link to protocol

    class Meta:
        model = Protocol
        fields = ("protocol_id", "session_date", "protocol_period")
        template_name = "speeches/table.html"
        empty_text = ("Für den eingegebenen Suchbegriff gibt es leider keine Ergebnisse.")
