from django.apps import AppConfig
from watson import search as watson


class SpeechesConfig(AppConfig):
    name = 'speeches'

    def ready(self):
        Protocol = self.get_model("Protocol")
        watson.register(Protocol, fields=["protocol_id",
                                          "protocol_period",
                                          "session_date_str"])

        Speech = self.get_model("Speech")
        watson.register(Speech,
                        fields=["speech_id",
                                "foreign_protocol__protocol_id",
                                "foreign_protocol__session_date_str",
                                "foreign_speaker__id",
                                "foreign_speaker__first_name",
                                "foreign_speaker__last_name"],
                        exclude=["speech_content"])
