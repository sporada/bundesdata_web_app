from django.db import models


class Protocol(models.Model):
    """
    This models contains the data about one protocol. Data will be imported from
    the XML protocols via the custom django-admin command import_protocols.py.
    Does not contain speeches. Speeches will be related to this model though.
    Only contains table of contents, metadata etc.
    """
    protocol_id = models.IntegerField(primary_key=True, verbose_name="Protokoll ID")
    protocol_period = models.IntegerField(verbose_name="Wahlperiode", null=True, blank=True)
    session_nr = models.IntegerField(verbose_name="Sitzungsnummer", null=True, blank=True)
    session_date = models.DateField(verbose_name="Datum")
    session_date_str = models.CharField(verbose_name="Datums String", max_length=12, blank=True, default=None, null=True)
    start_of_session = models.TimeField(null=True, verbose_name="Startuhrzeit")
    end_of_session = models.TimeField(null=True, verbose_name="Enduhrzeit")
    toc = models.TextField(verbose_name="Inhaltsverzeichnis")
    attachment = models.TextField(verbose_name="Anlagen")

    def __str__(self):
        return str(self.protocol_id) + " " + str(self.session_date)


class Speech(models.Model):
    """
    This models contains the data about one speech. Data will be imported from
    the XML protocols via the custom django-admin command import_speeches.py.
    """
    foreign_protocol = models.ForeignKey("Protocol", on_delete=models.CASCADE,
                                         verbose_name="Foreign Protokoll",
                                         default=None)
    speech_id = models.CharField(verbose_name="Rede ID", primary_key=True, max_length=14)
    previous_speech_id = models.CharField(verbose_name="Vorherige Rede ID", max_length=14, blank=True, default=None, null=True)
    next_speech_id = models.CharField(verbose_name="Nächste Rede ID", max_length=14, blank=True, default=None, null=True)
    speaker_type = models.CharField(verbose_name="Rolle des MdBs", max_length=50)
    foreign_speaker = models.ForeignKey("speakers.Speaker", on_delete=models.CASCADE,
                                        null=True, blank=True, verbose_name="MdB ID", )
    speech_content = models.TextField(verbose_name="Redeinhalt")  # import as XML element to string
    original_string = models.TextField(verbose_name="Original String")
    speaker_party_then = models.CharField(verbose_name="Damalige Partei", max_length=255, null=True, blank=True, default= None)

    def __str__(self):
        return (str(self.foreign_protocol) + " " + str(self.speech_id) + " "
                + self.speech_content[:20])
