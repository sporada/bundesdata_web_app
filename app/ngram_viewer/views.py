from django.shortcuts import render
from .charts import TimeChart, BarChart
from .forms import NgramForm, NgramFormSpeaker
from .ngram_search import NgramSearch, NgramSearchSpeaker
# import logging


def ngram_viewer_year(request):
    """
    This view creates the Ngram Viewer page per year.
    """
    # logger = logging.getLogger(__name__)
    if(request.method == "GET"):
        form = NgramForm(request.GET)
        if(form.is_valid()):
            clean_data = form.cleaned_data
            search = NgramSearch(clean_data)
            search.get_sub_querys()
            search.enhanced_search()
            search.query_sets_to_data()
            search.convert_to_data_set()
            line_chart = TimeChart()
            line_chart.get_datasets(data_sets=search.data_set)
            context = {"title": "Ngram Viewer für: " + clean_data["query"],
                       "form": form, "line_chart": line_chart}
            # logger.info(search.sub_querys_dict)
            # logger.info(search.filtered_sets_dict)
            # logger.info(search.raw_data)
            # logger.info(search.data_set)
            return render(request,
                          "ngram_viewer/ngram_viewer_year.html",
                          context)
        else:
            form = NgramForm()
            clean_data = {'query': 'Asyl, Kroatien, Krieg',
                          'case_sensitive': False,
                          'search_plus': False,
                          'ignore_missing': False,
                          'corpus_choice': 'lm_ns_year'}
            search = NgramSearch(clean_data)
            search.get_sub_querys()
            search.enhanced_search()
            search.query_sets_to_data()
            search.convert_to_data_set()
            line_chart = TimeChart()
            line_chart.get_datasets(data_sets=search.data_set)
            context = {"title": "Ngram Viewer pro Jahr für: " + clean_data["query"],
                       "form": form, "line_chart": line_chart}
            return render(request,
                          "ngram_viewer/ngram_viewer_year.html",
                          context)


def ngram_viewer_speaker(request):
    """
    This view creates the Ngram Viewer page per speaker.
    """
    if(request.method == "GET"):
        form = NgramFormSpeaker(request.GET)
        if(form.is_valid()):
            clean_data = form.cleaned_data
            search = NgramSearchSpeaker(clean_data)
            search.get_sub_querys()
            search.enhanced_search()
            search.query_sets_to_data()
            search.convert_to_data_set()
            speaker_data = search.get_speaker_name(search.data_set)
            bar_chart = BarChart(clean_data["range"])
            bar_chart.create_data(data_sets=speaker_data)
            bar_chart.get_datasets()
            bar_chart.get_labels()
            context = {"title": "Ngram Viewer für: " + clean_data["query"],
                       "form": form, "bar_chart": bar_chart}
            return render(request,
                          "ngram_viewer/ngram_viewer_speaker.html",
                          context)
        else:
            errors = form.errors
            form = NgramFormSpeaker()
            clean_data = {'query': 'Ausländer',
                          'case_sensitive': False,
                          'search_plus': False,
                          'ignore_missing': False,
                          'corpus_choice': 'lm_ns_speaker',
                          'range': '10'}
            search = NgramSearchSpeaker(clean_data)
            search.get_sub_querys()
            search.enhanced_search()
            search.query_sets_to_data()
            search.convert_to_data_set()
            speaker_data = search.get_speaker_name(search.data_set)
            bar_chart = BarChart(clean_data["range"])
            bar_chart.create_data(data_sets=speaker_data)
            bar_chart.get_datasets()
            bar_chart.get_labels()
            context = {"title": "Ngram Viewer pro MdB für: " + clean_data["query"],
                       "form": form, "bar_chart": bar_chart, "errors": errors}
            return render(request,
                          "ngram_viewer/ngram_viewer_speaker.html",
                          context)
