from django import forms


class NgramForm(forms.Form):
    """
    Describes and configures the input html form for the Ngram Viewer per year.
    """
    CORPUS_CHOICE = [('lm_ns_year', 'Lemmatisiert ohne Stoppwörter'),
                     ('tk_ws_year', 'Nicht lemmatisiert mit Stoppwörtern'), ]
    query = forms.CharField(label="Suche N-Gramme", max_length="200")
    case_sensitive = forms.BooleanField(label="case-sensitive", required=False)
    search_plus = forms.BooleanField(label="search-plus", required=False)
    ignore_missing = forms.BooleanField(label="fill-zeros", required=False)

    corpus_choice = forms.ChoiceField(label="Wählen Sie einen Corpus", choices=CORPUS_CHOICE)


class NgramFormSpeaker(forms.Form):
    """
    Describes and configures the input html form for the Ngram Viewer per speaker.
    """
    CORPUS_CHOICE = [('lm_ns_speaker', 'Lemmatisiert ohne Stoppwörter'),
                     ('tk_ws_speaker', 'Nicht lemmatisiert mit Stoppwörtern'), ]
    query = forms.CharField(label="Suche N-Gramm", max_length="200")
    case_sensitive = forms.BooleanField(label="case-sensitive", required=False)
    search_plus = forms.BooleanField(label="search-plus", required=False)
    ignore_missing = forms.BooleanField(label="fill-zeros", required=False)
    range = forms.IntegerField(label="Anzahl an Rednern")

    corpus_choice = forms.ChoiceField(label="Wählen Sie einen Corpus", choices=CORPUS_CHOICE)

    def clean_query(self):
        data = self.cleaned_data["query"]
        print(data)
        if(len(data.split(",")) > 1):
            raise forms.ValidationError("Es kann nur ein Ngramm gleichzeitig   \
                                         abgefragt werden.")
            print(data.split(",")[0])
            return data.split(",")[0]
        return data
