from django.urls import path
from . import views

"""
Url paths for all ngram_viewer views.
"""

urlpatterns = [
    path("pro-jahr/", views.ngram_viewer_year, name="ngram-viewer-jahr"),
    path("pro-mdb/", views.ngram_viewer_speaker, name="ngram-viewer-sprecher")
]
