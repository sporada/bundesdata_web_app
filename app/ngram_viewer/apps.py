from django.apps import AppConfig
from watson import search as watson


class NgramViewerConfig(AppConfig):
    name = 'ngram_viewer'
