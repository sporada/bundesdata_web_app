from jchart import Chart
from jchart.config import Axes, DataSet, rgba, Tick
from random import randint


class TimeChart(Chart):
    """
    Class to configure the N-Gramm Viewer line chart over time. The class function
    get_datasets() is used to get the data sets and creates one data set for
    each.
    """
    chart_type = "line"
    responsive = True
    scales = {
        'xAxes': [Axes(type='time', position="bottom")],
    }

    def __init__(self):
        super(TimeChart, self).__init__()
        self.data_sets = None

    def get_datasets(self, **kwargs):
        """
        Takes n number of data sets as an input and creates one data-line per
        data set.
        """
        if kwargs is not None:
            for key, value in kwargs.items():
                self.data_sets = value
        lable_names = []
        data_sets = []
        for dict in self.data_sets:
            for key, value in dict.items():
                lable_names.append(key)
                data_sets.append(value)
        data_set_objects = []
        for lable_name, data_set in zip(lable_names, data_sets):
            data_set_objects.append(DataSet(type="line",
                                            label=lable_name,
                                            borderColor=rgba(randint(0,255), randint(0,255), randint(0,255)),
                                            data=data_set,
                                            lineTension=0))
        return data_set_objects


class BarChart(Chart):
    """
    Class to configure the N-Gramm Viewer bar chart per speaker.
    """
    chart_type = "horizontalBar"
    responsive = True

    def __init__(self, speaker_range=10):
        super(BarChart, self).__init__()
        self.data_sets = None
        self.speaker_range = int(speaker_range)
        self.lable_names = []
        self.bar_data = []
        self.bar_names = []

    def get_labels(self):
        """
        Creates lables for the bar chart entries.
        """
        try:
            tmp_list = self.lable_names
            self.lable_names = sum(tmp_list, [])[:self.speaker_range]
        except TypeError as e:
            pass
        return self.lable_names

    def create_data(self, **kwargs):
        """
        Takes n numer of data sets but only one is passed because the
        Ngram Viewer per speaker is caped at one query at a time.
        """
        if kwargs is not None:
            for key, value in kwargs.items():
                self.data_sets = value
        for d in self.data_sets:
            entry_lable_names = []
            entry_bar_data = []
            entry_bar_name = []
            for key, value in d.items():
                for set in value:
                    entry_lable_names.append(set["x"])
                    entry_bar_data.append(set["y"])
                self.lable_names.append(entry_lable_names)
                entry_bar_name.append(key)
                self.bar_names.extend(entry_bar_name)
                entry_bar_data = entry_bar_data[:self.speaker_range]
                self.bar_data.append(entry_bar_data[:self.speaker_range])

    def get_datasets(self):
        """
        Takes the data sets from self.bar_data plus self.bar_names and creates
        one bar per speaker from this.
        """
        data_set_objects = []
        for bar_data, bar_name in zip(self.bar_data, self.bar_names):
            data_set_objects.append(DataSet(type="horizontalBar",
                                            label=bar_name,
                                            backgroundColor=rgba(randint(0,255), randint(0,255), randint(0,255)),
                                            data=bar_data[:self.speaker_range]))
        return data_set_objects
