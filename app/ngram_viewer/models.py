from django.db import models

"""
Contains all the models for the diffferent ngram corpora. The models are
automatically generated with the utils/create_ngram_models.py script. One model
holds one kind of ngram. The name of the model follows a pattern describing the
specific kind of ngam.
For example: KeyA_TwoGram_lm_ns_year --> This model will create a table
contianing all lemmatized (lm) 2-grams without stopwords (ns) per year starting
with the letter "A" or "a".
For example: Key_Non_ASCII_ThreeGram_tk_ws_speaker --> This model will create a
table containing all tokenized (tk) 3-grams with stopwords (ws) per speaker
starting with any non ASCII letter like ü, ö, ä or é.

The Idea behind these splits and a single table for every kind of ngram is to
minimize search times for the user. It would have been possible to create a table
for every 1-gram, 2-gram etc. But these would have benn pretty long (100 millions
 of) rows.
"""


class Key0_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key1_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key2_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key3_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key4_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key5_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key6_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key7_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key8_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key9_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyA_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyB_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyC_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyD_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyE_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyF_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyG_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyH_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyI_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyJ_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyK_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyL_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyM_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyN_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyO_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyP_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyQ_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyR_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyS_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyT_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyU_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyV_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyW_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyX_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyY_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyZ_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key_Non_ASCII_OneGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key0_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key1_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key2_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key3_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key4_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key5_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key6_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key7_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key8_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key9_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyA_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyB_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyC_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyD_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyE_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyF_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyG_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyH_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyI_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyJ_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyK_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyL_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyM_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyN_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyO_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyP_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyQ_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyR_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyS_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyT_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyU_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyV_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyW_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyX_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyY_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyZ_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key_Non_ASCII_TwoGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key0_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key1_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key2_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key3_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key4_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key5_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key6_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key7_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key8_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key9_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyA_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyB_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyC_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyD_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyE_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyF_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyG_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyH_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyI_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyJ_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyK_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyL_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyM_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyN_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyO_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyP_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyQ_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyR_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyS_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyT_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyU_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyV_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyW_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyX_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyY_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyZ_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key_Non_ASCII_ThreeGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key0_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key1_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key2_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key3_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key4_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key5_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key6_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key7_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key8_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key9_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyA_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyB_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyC_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyD_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyE_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyF_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyG_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyH_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyI_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyJ_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyK_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyL_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyM_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyN_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyO_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyP_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyQ_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyR_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyS_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyT_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyU_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyV_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyW_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyX_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyY_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyZ_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key_Non_ASCII_FourGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key0_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key1_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key2_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key3_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key4_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key5_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key6_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key7_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key8_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key9_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyA_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyB_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyC_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyD_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyE_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyF_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyG_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyH_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyI_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyJ_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyK_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyL_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyM_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyN_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyO_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyP_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyQ_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyR_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyS_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyT_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyU_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyV_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyW_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyX_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyY_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyZ_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key_Non_ASCII_FiveGram_lm_ns_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key0_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key1_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key2_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key3_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key4_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key5_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key6_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key7_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key8_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key9_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyA_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyB_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyC_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyD_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyE_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyF_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyG_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyH_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyI_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyJ_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyK_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyL_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyM_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyN_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyO_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyP_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyQ_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyR_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyS_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyT_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyU_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyV_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyW_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyX_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyY_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyZ_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key_Non_ASCII_OneGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key0_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key1_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key2_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key3_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key4_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key5_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key6_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key7_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key8_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key9_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyA_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyB_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyC_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyD_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyE_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyF_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyG_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyH_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyI_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyJ_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyK_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyL_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyM_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyN_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyO_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyP_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyQ_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyR_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyS_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyT_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyU_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyV_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyW_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyX_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyY_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyZ_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key_Non_ASCII_TwoGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key0_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key1_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key2_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key3_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key4_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key5_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key6_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key7_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key8_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key9_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyA_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyB_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyC_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyD_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyE_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyF_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyG_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyH_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyI_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyJ_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyK_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyL_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyM_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyN_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyO_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyP_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyQ_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyR_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyS_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyT_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyU_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyV_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyW_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyX_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyY_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyZ_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key_Non_ASCII_ThreeGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key0_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key1_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key2_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key3_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key4_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key5_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key6_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key7_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key8_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key9_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyA_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyB_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyC_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyD_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyE_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyF_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyG_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyH_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyI_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyJ_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyK_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyL_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyM_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyN_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyO_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyP_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyQ_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyR_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyS_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyT_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyU_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyV_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyW_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyX_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyY_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyZ_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key_Non_ASCII_FourGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key0_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key1_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key2_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key3_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key4_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key5_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key6_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key7_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key8_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key9_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyA_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyB_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyC_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyD_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyE_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyF_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyG_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyH_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyI_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyJ_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyK_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyL_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyM_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyN_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyO_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyP_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyQ_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyR_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyS_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyT_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyU_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyV_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyW_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyX_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyY_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyZ_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key_Non_ASCII_FiveGram_tk_ws_year(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key0_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key1_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key2_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key3_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key4_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key5_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key6_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key7_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key8_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key9_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyA_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyB_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyC_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyD_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyE_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyF_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyG_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyH_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyI_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyJ_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyK_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyL_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyM_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyN_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyO_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyP_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyQ_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyR_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyS_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyT_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyU_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyV_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyW_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyX_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyY_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyZ_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key_Non_ASCII_OneGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key0_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key1_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key2_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key3_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key4_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key5_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key6_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key7_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key8_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key9_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyA_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyB_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyC_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyD_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyE_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyF_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyG_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyH_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyI_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyJ_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyK_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyL_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyM_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyN_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyO_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyP_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyQ_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyR_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyS_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyT_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyU_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyV_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyW_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyX_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyY_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyZ_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key_Non_ASCII_TwoGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key0_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key1_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key2_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key3_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key4_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key5_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key6_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key7_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key8_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key9_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyA_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyB_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyC_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyD_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyE_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyF_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyG_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyH_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyI_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyJ_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyK_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyL_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyM_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyN_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyO_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyP_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyQ_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyR_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyS_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyT_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyU_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyV_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyW_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyX_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyY_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyZ_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key_Non_ASCII_ThreeGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key0_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key1_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key2_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key3_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key4_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key5_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key6_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key7_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key8_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key9_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyA_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyB_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyC_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyD_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyE_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyF_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyG_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyH_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyI_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyJ_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyK_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyL_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyM_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyN_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyO_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyP_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyQ_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyR_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyS_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyT_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyU_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyV_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyW_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyX_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyY_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyZ_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key_Non_ASCII_FourGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key0_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key1_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key2_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key3_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key4_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key5_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key6_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key7_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key8_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key9_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyA_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyB_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyC_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyD_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyE_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyF_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyG_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyH_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyI_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyJ_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyK_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyL_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyM_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyN_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyO_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyP_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyQ_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyR_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyS_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyT_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyU_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyV_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyW_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyX_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyY_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyZ_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key_Non_ASCII_FiveGram_lm_ns_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key0_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key1_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key2_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key3_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key4_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key5_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key6_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key7_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key8_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key9_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyA_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyB_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyC_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyD_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyE_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyF_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyG_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyH_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyI_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyJ_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyK_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyL_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyM_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyN_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyO_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyP_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyQ_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyR_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyS_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyT_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyU_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyV_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyW_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyX_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyY_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyZ_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key_Non_ASCII_OneGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='OneGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key0_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key1_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key2_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key3_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key4_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key5_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key6_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key7_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key8_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key9_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyA_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyB_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyC_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyD_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyE_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyF_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyG_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyH_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyI_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyJ_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyK_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyL_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyM_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyN_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyO_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyP_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyQ_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyR_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyS_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyT_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyU_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyV_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyW_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyX_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyY_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyZ_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key_Non_ASCII_TwoGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='TwoGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key0_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key1_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key2_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key3_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key4_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key5_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key6_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key7_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key8_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key9_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyA_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyB_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyC_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyD_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyE_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyF_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyG_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyH_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyI_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyJ_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyK_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyL_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyM_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyN_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyO_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyP_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyQ_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyR_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyS_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyT_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyU_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyV_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyW_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyX_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyY_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyZ_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key_Non_ASCII_ThreeGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='ThreeGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key0_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key1_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key2_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key3_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key4_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key5_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key6_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key7_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key8_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key9_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyA_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyB_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyC_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyD_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyE_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyF_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyG_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyH_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyI_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyJ_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyK_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyL_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyM_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyN_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyO_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyP_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyQ_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyR_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyS_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyT_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyU_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyV_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyW_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyX_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyY_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyZ_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key_Non_ASCII_FourGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FourGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key0_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key1_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key2_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key3_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key4_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key5_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key6_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key7_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key8_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key9_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyA_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyB_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyC_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyD_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyE_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyF_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyG_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyH_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyI_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyJ_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyK_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyL_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyM_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyN_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyO_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyP_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyQ_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyR_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyS_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyT_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyU_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyV_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyW_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyX_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyY_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class KeyZ_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)


class Key_Non_ASCII_FiveGram_tk_ws_speaker(models.Model):
    ngram = models.CharField(verbose_name='FiveGram',
                             max_length=255,
                             default=None,
                             null=True,
                             blank=True)
    key = models.CharField(max_length=255)
    count = models.IntegerField()

    def __str__(self):
        return str(self.ngram) + " " + str(self.key)
